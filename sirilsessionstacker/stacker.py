"""Session stacker command."""

import os

import yaml
from astropy.io import fits

from sirilsessionstacker.sirilhelper import SirilHelper


def load_common_metadata(common_dir):
    """Load metadata from common directory."""
    common_metadata_filepath = os.path.join(common_dir, "metadata.yml")
    if os.path.isfile(common_metadata_filepath):
        with open(common_metadata_filepath, "r", encoding="utf-8") as filepointer:
            print(f"Loaded common metadata from {common_metadata_filepath}")
            common_metadata = yaml.safe_load(filepointer)
    else:
        print("No metadata.yml file found. Scanning common directory.")
        common_metadata = {}
        # Scan directories and add files to manually created common metadata
        for frame_type in ["bias", "dark"]:
            frame_dir = os.path.join(common_dir, frame_type)
            filelist = os.listdir(frame_dir)
            for filename in filelist:
                extension = os.path.splitext(filename)[1].lower()
                if extension in [".fit", ".fits"]:
                    file_path = os.path.join(frame_dir, filename)
                    if frame_type == "bias":
                        print(f"Adding bias file: {file_path}")
                        add_bias_to_common_metadata(file_path, common_metadata)
                    else:
                        print(f"Adding dark file: {file_path}")
                        add_dark_to_common_metadata(file_path, common_metadata)
    return common_metadata


def check_fits_files(frame_dir, at_least=4):
    """Check if there are fit files in given frame directory."""
    num = 0
    filelist = os.listdir(frame_dir)
    for filename in filelist:
        extension = os.path.splitext(filename)[1].lower()
        if extension in [".fit", ".fits"]:
            num += 1
            if num >= at_least:
                return True
    return False


def find_frame_dirs(frame_dir):
    """Find suitable frame subdirectories within given directory."""
    # Get all subdirs
    frame_dirs = []

    # Check current dir
    if check_fits_files(frame_dir):
        frame_dirs.append(frame_dir)

    # Check subdirectories
    try:
        dirpath, dirnames, _filenames = os.walk(frame_dir).__next__()
    except StopIteration:
        return frame_dirs

    for dirname in dirnames:
        frame_dir_path = os.path.join(os.path.abspath(dirpath), dirname)
        # Check if the directory has FIT files
        if check_fits_files(frame_dir_path):
            frame_dirs.append(frame_dir_path)
    return frame_dirs


def scan_session_dir(session_dir):
    """Scan a session directory and get the different subdirectories to be processed."""
    process_dirs = {"bias": [], "dark": [], "flat": [], "light": []}
    for frame_dir_name in process_dirs:
        session_dir = os.path.abspath(session_dir)
        frame_dir = os.path.join(session_dir, frame_dir_name)
        if os.path.isdir(frame_dir):
            process_dirs[frame_dir_name] = find_frame_dirs(frame_dir)
    return process_dirs


def get_frame_metadata(filepath):
    """Get frame metadata information."""
    hdu = fits.open(filepath)
    if len(hdu) > 0:
        data = hdu[0].header  # pylint: disable=no-member
    else:
        data = {}
    metadata = {
        "camera": data.get("INSTRUME", None),
        "gain": data.get("GAIN", None),
        "exposure": data.get("EXPTIME", None),
        "temperature": data.get("CCD-TEMP", None),
        "cfa": "BAYERPAT" in data,
    }
    return metadata


def get_frame_matching_bias(frame_data, common_metadata):
    """Get a maching master bias for given frame data."""
    camera = frame_data["camera"]
    gain = frame_data["gain"]
    if gain:
        gain = int(float(gain))
    if camera and gain:
        try:
            return common_metadata["cameras"][camera]["bias"][gain]
        except KeyError:
            pass
    return None


def get_frame_matching_dark(frame_data, common_metadata):
    """Get a maching master dark for given frame data."""
    camera = frame_data["camera"]
    gain = frame_data["gain"]
    if gain:
        gain = int(float(gain))
    exposure = frame_data["exposure"]
    if exposure:
        exposure = int(float(exposure))
    temperature = frame_data["temperature"]
    if temperature:
        temperature = int(float(temperature))
    if camera and gain and exposure and temperature:
        matching_dark = f"{gain}_{exposure}_{temperature}"
        try:
            return common_metadata["cameras"][camera]["dark"][matching_dark]
        except KeyError:
            pass
    return None


def get_frame_from_dir(frame_dir):
    """Get the first frame found in given directory."""
    filelist = os.listdir(frame_dir)
    for filename in filelist:
        extension = os.path.splitext(filename)[1].lower()
        if extension in [".fit", ".fits"]:
            return os.path.join(frame_dir, filename)
    return None


def get_matching_calibration_frames(frame_dir, common_metadata, common_dir):
    """Get the correct calibration frames for given frame directory."""
    matching_bias = None
    matching_dark = None
    frame_filepath = get_frame_from_dir(frame_dir)
    frame_data = get_frame_metadata(frame_filepath)
    if common_dir:
        matching_dark = get_frame_matching_dark(frame_data, common_metadata)
        if matching_dark:
            matching_dark = os.path.join(common_dir, os.path.splitext(matching_dark)[0])
        else:
            matching_bias = get_frame_matching_bias(frame_data, common_metadata)
            if matching_bias:
                matching_bias = os.path.join(
                    common_dir, os.path.splitext(matching_bias)
                )
    return {"bias": matching_bias, "dark": matching_dark, "cfa": frame_data["cfa"]}


def process_bias(siril, directory, sequence_name):
    """Process bias frames."""
    siril.stack_master(directory=directory, name=sequence_name, normalization="no")


def process_dark(siril, directory, sequence_name):
    """Process dark frames."""
    siril.stack_master(directory=directory, name=sequence_name, normalization="no")


def process_flat(siril, directory, sequence_name, calibration_frames):
    """Process flat frames."""
    bias = calibration_frames["bias"]
    dark = calibration_frames["dark"]
    flat_name = siril.stack_flat(
        directory=directory, name=sequence_name, bias=bias, dark=dark
    )
    return f"{flat_name}_stacked"


def process_light(siril, directory, sequence_name, calibration_frames, flat_frame):
    """Process light frames."""
    dark = calibration_frames["dark"]
    cfa = calibration_frames["cfa"]
    siril.stack_light(
        directory=directory, name=sequence_name, dark=dark, flat=flat_frame, cfa=cfa
    )


def find_flat_frame(common_metadata, basename):
    """Find a suitable flat frame."""
    if "flat" in common_metadata:
        flat_name = f"flat_{basename}_stacked"
        if f"pp_{flat_name}" in common_metadata["flat"]:
            return f"pp_{flat_name}"
        if flat_name in common_metadata["flat"]:
            return flat_name
    return None


def add_bias_to_common_metadata(bias_path, common_metadata):
    """Add master bias entry to common metadata."""
    frame_data = get_frame_metadata(bias_path)
    camera = frame_data["camera"]
    gain = frame_data["gain"]
    if camera:
        if camera not in common_metadata:
            common_metadata[camera] = {}
        if "bias" not in common_metadata[camera]:
            common_metadata[camera]["bias"] = {}
        if frame_data["gain"]:
            common_metadata[camera]["bias"][gain] = bias_path


def add_dark_to_common_metadata(dark_path, common_metadata):
    """Add master dark entry to common metadata."""
    frame_data = get_frame_metadata(dark_path)
    camera = frame_data["camera"]
    if camera:
        if camera not in common_metadata:
            common_metadata[camera] = {}
        if "dark" not in common_metadata[camera]:
            common_metadata[camera]["dark"] = {}
        if frame_data["gain"] and frame_data["exposure"] and frame_data["temperature"]:
            dark_identifier = \
                f'{frame_data["gain"]}_{frame_data["exposure"]}_{frame_data["temperature"]}'
            common_metadata[camera]["dark"][dark_identifier] = dark_path


def main(session_dir, common_dir, siril_path, verbose):
    # pylint: disable=too-many-locals
    """Run main script."""
    session_dir = os.path.abspath(session_dir)
    # Scan session directory
    process_dirs = scan_session_dir(session_dir)
    # Load common data
    if common_dir:
        common_dir = os.path.abspath(common_dir)
        common_metadata = load_common_metadata(common_dir)
    else:
        common_metadata = {}

    # Initialize Siril
    siril = SirilHelper(
        session_dir,
        siril_path=siril_path,
        verbose=verbose)

    # Process bias frames
    for directory in process_dirs["bias"]:
        print(f"* Processing bias frames at {directory}")
        basename = os.path.basename(directory)
        sequence_name = f"bias_{basename}"
        process_bias(siril, directory, sequence_name)
        # Add bias to common metadata
        bias_filename = f"{sequence_name}_stacked.fit"
        bias_path = os.path.join(siril.process_dir, bias_filename)
        add_bias_to_common_metadata(bias_path, common_metadata)

    # Process dark frames
    for directory in process_dirs["dark"]:
        print(f"* Processing dark frames at {directory}")
        basename = os.path.basename(directory)
        sequence_name = f"dark_{basename}"
        process_dark(siril, directory, sequence_name)
        # Add dark to common metadata
        dark_filename = f"{sequence_name}_stacked.fit"
        dark_path = os.path.join(siril.process_dir, dark_filename)
        add_dark_to_common_metadata(dark_path, common_metadata)

    # Process flat frames
    for directory in process_dirs["flat"]:
        print(f"* Processing flat frames at {directory}")
        basename = os.path.basename(directory)
        sequence_name = f"flat_{basename}"
        calibration_frames = get_matching_calibration_frames(
            directory, common_metadata, common_dir
        )
        if calibration_frames["dark"]:
            print(f"* Using master dark {calibration_frames['dark']} for flat calibration")
        elif calibration_frames["bias"]:
            print(f"* Using master bias {calibration_frames['bias']} for flat calibration")
        flat_name = process_flat(
            siril=siril,
            directory=directory,
            sequence_name=sequence_name,
            calibration_frames=calibration_frames,
        )
        # Add flat to common metadata
        if "flat" not in common_metadata:
            common_metadata["flat"] = []
        if flat_name:
            common_metadata["flat"].append(flat_name)

    # Process light frames
    for directory in process_dirs["light"]:
        print(f"* Processing light frames at {directory}")
        basename = os.path.basename(directory)
        sequence_name = f"light_{basename}"
        calibration_frames = get_matching_calibration_frames(
            directory, common_metadata, common_dir
        )
        if calibration_frames["dark"]:
            print(f"* Using master dark {calibration_frames['dark']} for light calibration")
        flat_frame = find_flat_frame(common_metadata, basename)
        if flat_frame:
            print(f"* Using master flat {flat_frame} for light calibration")
        process_light(
            siril=siril,
            directory=directory,
            sequence_name=sequence_name,
            calibration_frames=calibration_frames,
            flat_frame=flat_frame,
        )

    # Close Siril instance
    siril.quit()
