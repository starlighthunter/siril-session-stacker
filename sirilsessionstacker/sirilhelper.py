"""
Siril helper class.

https://siril.org/tutorials/pysiril/
https://free-astro.org/index.php/Siril:Commands

"""

import os

from pysiril.siril import Siril
from pysiril.wrapper import Wrapper


class SirilHelper:
    """Siril helper class."""

    def __init__(self, session_dir, siril_path, verbose=False):
        """Siril helper initialization."""
        self.siril_path = siril_path
        self.verbose = verbose
        self.process_dir = os.path.join(session_dir, "process")
        if not os.path.isdir(self.process_dir):
            os.makedirs(self.process_dir)
        self.initialize_siril()

    def initialize_siril(self):
        """Open a connection with Siril and configure it."""
        # Open Siril
        self.siril = Siril(siril_exe=self.siril_path)
        if not self.verbose:
            self.siril.MuteSiril(bValue=True)
        self.siril_cmd = Wrapper(self.siril)
        self.siril.Open()
        # Set siril preferences
        self.siril_cmd.set16bits()
        self.siril_cmd.setext("fit")
        print("\n----------------------------------------------\n")

    def quit(self):
        """Close Siril connection."""
        print("\n----------------------------------------------\n")
        self.siril_cmd.close()
        self.siril.Close()

    def convert_fits_sequence(self, directory, name, debayer=False):
        """Create a fits sequence from the frames in given directory."""
        print(f"* Converting files in {directory} into sequence {name}")
        self.siril_cmd.cd(directory)
        self.siril_cmd.convert(name, out=self.process_dir, debayer=debayer, fitseq=True)

    def stack_master(self, directory, name, normalization):
        """Stack master frame using the frames in given directory."""
        self.convert_fits_sequence(directory, name)
        self.siril_cmd.cd(self.process_dir)
        print(f"* Stacking master {name}")
        self.siril_cmd.stack(
            name, type="rej", sigma_low=3, sigma_high=3, norm=normalization
        )

    def stack_flat(self, directory, name, bias=None, dark=None):
        """Stack flat frames in given directory."""
        self.convert_fits_sequence(directory, name)
        self.siril_cmd.cd(self.process_dir)
        if bias or dark:
            print(f"* Preprocessing sequence {name}")
            self.siril_cmd.preprocess(name, bias=bias, dark=dark)
            name = f"pp_{name}"
        self.stack_master(directory=directory, name=name, normalization="mul")
        return name

    def stack_light(self, directory, name, dark, flat, cfa=True):
        # pylint: disable=too-many-arguments
        """Stack light frames in given directory."""
        if dark or flat:
            self.convert_fits_sequence(directory, name)
            self.siril_cmd.cd(self.process_dir)
            # Preprocessing
            if dark or flat:
                print(f"* Preprocessing sequence {name}")
                self.siril_cmd.preprocess(
                    name,
                    dark=dark,
                    flat=flat,
                    cfa=cfa,
                    equalize_cfa=cfa and flat is not None,
                    debayer=cfa,
                )
                name = f"pp_{name}"
        else:
            self.convert_fits_sequence(directory, name, debayer=cfa)
            self.siril_cmd.cd(self.process_dir)
        # Registering
        print(f"* Registering sequence {name}")
        self.siril_cmd.register(name)
        # Stacking
        print(f"* Stacking master {name}")
        self.siril_cmd.stack(
            f"r_{name}",
            type="rej",
            sigma_low=3,
            sigma_high=3,
            norm="addscale",
            output_norm=True,
        )
