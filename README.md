# Automated Siril integrator

Script for automated integration in Siril

It uses a specific session directory structure to determine which frames
will be calibrated and stacked.

It also uses a common directory that may contain previously stacked master
frames to reuse them.


## Session directory structure

Session directory is simple: For each frame type (bias, dark, flat, light),
a directory with files, or subdirectories for different versions of that
frame type.

Bias frames will be stacked and used in future calibration of matching gain
flats if not dark frames matching the flats (flat darks).

Dark frames will be stacked and used in future calibration of matching gain,
exposure and temperature flats and lights.

Flat frames will be calibrated with an available dark matching gain,
exposure and temperature, and if it is not any match, with a bias with the
same gain. If no bias either, then flat frames will not be calibrated.

Light frames will be calibrated with matching gain, exposure and temperature
dark frame, and also with a flat frame that will be selected by matching the
name of the subfolder, or just the toplevel flat.

For example:

If there is a ***light/blue/*** subdirectory, and there is a ***flat/blue/***
subdirectory then the flat resulting of stacking the frames in the ***flat/blue/***
subdirectory will be used to calibrate the light frames in ***light/blue/***
subdirectory.

If there are flat frames in ***flat/***, the light frames in ***light/*** will
be calibrated using the flat resulting of stacking the frames in the ***flat/***.

When calibrating lights, if the frames are OSC frames, they will be automatically
debayered.

All the sequences and stacked files will be stored in a ***process/*** subdirectory
located on the root of the session directory.

Bias, dark and flat frames are optional. If any of them are not present, they
just will not be used.

Here it is a sample of a session directory.

```
-- Session dir  \
                |-- bias  --\
                |           |-- gain-1 --\
                |           |            |-- bias_gain-1-1.fit
                |           |            |-- bias_gain-1-2.fit
                |           |            \-- bias_gain-1-n.fit
                |           |
                |           |-- gain-2 --\
                |           |            |-- bias_gain-2-1.fit
                |           |            |-- bias_gain-2-2.fit
                |           |            \-- bias_gain-2-n.fit
                |           |
                |           |-- gain-n --\
                |           |            |-- bias_gain-n-1.fit
                |           |            |-- bias_gain-n-2.fit
                |           |            \-- bias_gain-n-n.fit
                |           |
                |           |-- bias-1.fit
                |           |-- bias-2.fit
                |           \-- bias-n.fit
                |
                |-- dark  --\
                |           |-- gain_exp_temp-1 --\
                |           |                     |-- dark_gain-exp-temp-1-1.fit
                |           |                     |-- dark_gain-exp-temp-1-2.fit
                |           |                     \-- dark_gain-exp-temp-1-n.fit
                |           |
                |           |-- gain-exp-temp-2 --\
                |           |                     |-- dark_gain-exp-temp-2-1.fit
                |           |                     |-- dark_gain-exp-temp-2-2.fit
                |           |                     \-- dark_gain-exp-temp-2-n.fit
                |           |
                |           |-- gain-exp-temp-n --\
                |           |                     |-- dark_gain-exp-temp-n-1.fit
                |           |                     |-- dark_gain-exp-temp-n-2.fit
                |           |                     \-- dark_gain-exp-temp-n-n.fit
                |           |
                |           |-- dark-1.fit
                |           |-- dark-2.fit
                |           \-- dark-n.fit
                |
                |-- flat  --\
                |           |-- filter-1 --\
                |           |              |-- flat_filter-1-1.fit
                |           |              |-- flat_filter-1-2.fit
                |           |              \-- flat_filter-1-n.fit
                |           |
                |           |-- filter-2 --\
                |           |              |-- flat_filter-2-1.fit
                |           |              |-- flat_filter-2-2.fit
                |           |              \-- flat_filter-2-n.fit
                |           |
                |           |-- filter-n --\
                |           |              |-- flat_filter-n-1.fit
                |           |              |-- flat_filter-n-2.fit
                |           |              \-- flat_filter-n-n.fit
                |           |
                |           |-- flat-1
                |           |-- flat-2
                |           \-- flat-n
                |
                \-- light --\
                            |-- filter-1 --\
                            |              |-- light_filter-1-1.fit
                            |              |-- light_filter-1-2.fit
                            |              \-- light_filter-1-n.fit
                            |
                            |-- filter-2 --\
                            |              |-- light_filter-2-1.fit
                            |              |-- light_filter-2-2.fit
                            |              \-- light_filter-2-n.fit
                            |
                            |-- filter-n --\
                            |              |-- light_filter-n-1.fit
                            |              |-- light_filter-n-2.fit
                            |              \-- light_filter-n-n.fit
                            |
                            |-- light-1.fit
                            |-- light-2.fit
                            \-- light-n.fit

```

## Common directory structure

```
-- Common dir   \
                |-- bias
                |-- dark
                |-- pkgs
                \-- metadata.yml
```

- **bias**: Master bias files
- **dark**: Master dark files
- **pkg**: Siril.AppImage executable and other package files
- **metadata.yml**: Metadata file

The metadata.yml file contains the description of the data contained in the
common directory.

```yml
# Common metadata information

cameras:
  # Camera models
  "ZWO ASI294MC Pro":
    # Master bias files
    bias:
      # Gain: Filename
      0: "bias/2019-10_ASI209MC-P_master-bias_G0.fit"
      120: "bias/2019-10_ASI209MC-P_master-bias_G120.fit"

    # Master dark files
    dark:
      # "Gain_Eposure_Temp: Filename
      "120_10_-10": "dark/2022-05_ASI294MC-Pro_master-dark_G120_10s_T-10c.fit"
      "120_60_-10": "dark/2021-10_ASI294MC-Pro_master-dark_G120_60s_T-10c.fit"
      "120_120_-10": "dark/2021-10_ASI294MC-Pro_master-dark_G120_120s_T-10c.fit"
```

If the metadata file is not present, the script will try to read any files in the
***bias/*** and ***dark/*** directories and use them. For that, the files need to
have correct FITS headers for ***INSTRUMENT***, ***GAIN***, ***EXPTIME***,
***CCD-TEMP*** and ***BAYERPAT*** fields.

## Installation

The only prerequisite you need is to have a compatible version of Siril installed on your system. If it is not automatically detected by the script, you can specify it as a parameter (see the **Usage** section).

After that, you can just install this package with the following command:

```bash
$ pip install --user .
```

## Usage

Once you have installed the package you just have to run the ***session_stacker***
command.

```bash
$ session_stacker 
usage: session_stacker [-h] [-s SIRILEXEC] SESSIONDIR [COMMONDIR]
session_stacker: error: the following arguments are required: SESSIONDIR
```

To start the stacking and integration of a session just execute the command specifying
the session directory that contains your frames.

```bash
$ session_stacker /path/to/session_dir
```

If you also have a common directory with some master frames that could be useful in the calibration process, you can specify it after the session directory.

```bash
$ session_stacker /path/to/session_dir /path/to/common_dir
```

If you do not have Siril installed or do not have the siril executable at ***/usr/local/bin/siril***,
you can specify the Siril executable path using the ***-s*** or ***--sirilexec*** parameter. You can
set it to an installed Siril executable or to an AppImage file.

```bash
$ session_stacker -s Siril-1.0.3-x86_64.AppImage /path/to/session_dir /path/to/common_dir
```



## References

- https://siril.org/tutorials/pysiril/
- https://free-astro.org/index.php/Siril:Commands
- https://gitlab.com/free-astro/pysiril/-/tree/master/
