#!/usr/bin/python3
"""
Session stacker command
"""

from pyclihelper.command import BaseCommand

from sirilsessionstacker import stacker


class SessionStackerCommand(BaseCommand):
    """Session stacker command."""
    
    command_definition = {
        "name": "sirilsessionstacker",
        "desc": "Script for automated integration in Siril",
        "userconfig": True,
        "args": {
            "sessiondir": {
                "desc": "Astrophotography session directory",
            },
            "commondir": {
                "desc": "Common data directory",
                "default": None
            },
            "sirilexec": {
                "shortcut": "s",
                "desc": "Siril executable path",
            },
            "verbose": {
                "shortcut": "v",
                "desc": "Siril verbose output",
                "type": bool,
                "default": False,
            }
        }
    }

    def run_command(self, args):
        """Run command."""
        stacker.main(args.sessiondir, args.commondir, args.sirilexec, args.verbose)


if __name__ == "__main__":
    cmd = SessionStackerCommand()
    cmd.run()
